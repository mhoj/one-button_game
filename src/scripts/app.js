'use strict';

// helper function for copying configs
function deepClone(obj, map = new WeakMap()) {
    if (obj instanceof RegExp) return new RegExp(obj);
    if (obj instanceof Date) return new Date(obj);

    if (obj == null || typeof obj != 'object') return obj;
    if (map.has(obj)) {
        return map.get(obj);
    }
    let t = new obj.constructor();
    map.set(obj, t);
    for (let key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            t[key] = deepClone(obj[key], map);
        }
    }
    return t;
}

function genRandomInteger(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

// calculate distance from point p to rectangle rect
function getDistance(p, rect) {
    var dx = Math.max(rect.min.x - p.x, 0, p.x - rect.max.x);
    var dy = Math.max(rect.min.y - p.y, 0, p.y - rect.max.y);
    return Math.sqrt(dx*dx + dy*dy);
}

function drawRotatedRect(context, x, y, w, h, fillStyle, arc){
    // first save the untranslated/unrotated
    context.save();

    context.beginPath();
    // move the rotation point to the center of the rect
    context.translate( x + w / 2, y + h / 2);
    // rotate the rect
    context.rotate(arc); // deg * Math.PI / 180

    // draw the rect on the transformed context
    // Note: after transforming [0,0] is visually [x,y]
    //       so the rect needs to be offset accordingly when drawn
    context.rect( -w/2, -h/2, w,h);

    context.fillStyle = fillStyle;
    context.fill();

    // restore the context to its untranslated/unrotated state
    context.restore();

}

// scale[x, y]
// base should be center/top/bottom
function drawScaledRect(context, x, y, w, h, fillStyle, scale, base='center') {
    // first save the untranslated/unrotated
    context.save();

    context.beginPath();
    // move the rotation point to the center of the rect
    switch (base) {
    case 'center': {
        context.translate( x + w / 2, y + h / 2);
        break;
    }
    case 'top': {
        context.translate( x + w / 2, y);
        break;
    }
    case 'bottom': {
        context.translate( x + w / 2, y + h);
        break;
    }
    }
    // scale the rect
    context.scale(scale[0], scale[1]);
    // draw
    context.rect( -w/2, -h/2, w,h);

    context.fillStyle = fillStyle;
    context.fill();

    // restore the context to its untranslated/unrotated state
    context.restore();
    
}

function shiftCanvasHorizontal(context, delta) {
    const imageData = context.getImageData(0, 0, context.canvas.width - delta, context.canvas.height);
    context.putImageData(imageData, delta, 0);
    context.clearRect(delta, 0, delta, context.canvas.height);
}

class Player {
    constructor(config) {
        // states
        this.STATES = {
            JUMP_UP: 'jumpup',
            JUMP_DOWN: 'jumpdown',
            FORWARD_TOP: 'forwardtop',
            FORWARD_BOTTOM: 'forwardbottom'
        };
        this.state = this.STATES.FORWARD_BOTTOM;
        // charge
        this.isCharging = false;
        this.chargedTime = 0;
        this.maxChargedTime = config.maxChargedTime;
        // config
        this.dimension = {...config.dimension};
        this.boundary = {...config.boundary};
        this.position = {...config.position};
        this.vilocity = {...config.vilocity};
        this.gravity = config.gravity;
        this.canvasConfig = {...config.canvasConfig};
        this.fillStyle = config.fillStyle;
    }

    getBoundingBox() {
        const halfWidth = this.dimension.width / 2;
        return {
            position: {
                x: this.position.x + halfWidth,
                y: this.position.y + this.dimension.height / 2
            },
            radius: halfWidth
        };
    }

    getChargedRate() {
        return this.isCharging ? Math.min(1.0, this.chargedTime / this.maxChargedTime) : 0.0;
    }

    getSpeed() {
        return this.isCharging ?
            -((1 - this.getChargedRate()) * (-this.vilocity.x + this.vilocity.runSpeedMin)
            - this.vilocity.runSpeedMin) : this.vilocity.x;
    }

    draw(context, shiftCanvas) {
        if (shiftCanvas) {
            // shift everything to the left:
            context.fillStyle = 'rgba(255,255,255,0.1)';
            context.fillRect(0, 0, this.canvasConfig.width, this.canvasConfig.height);
            shiftCanvasHorizontal(context, this.getSpeed());
        }
        context.fillStyle = this.fillStyle;
        switch (this.state) {
        case this.STATES.JUMP_UP: {
            const totalDistance = this.boundary.bottom - this.boundary.top;
            const distance = this.position.y - this.boundary.top;
            const arc = Math.PI * (distance / totalDistance);
            drawRotatedRect(context, this.position.x, this.position.y,
                this.dimension.width, this.dimension.height, this.fillStyle, arc);
            break;
        }
        case this.STATES.JUMP_DOWN: {
            const totalDistance = this.boundary.bottom - this.boundary.top;
            const distance = this.boundary.bottom- this.position.y;
            const arc = Math.PI * (distance / totalDistance);
            drawRotatedRect(context, this.position.x, this.position.y,
                this.dimension.width, this.dimension.height, this.fillStyle, arc);
            break;
        }
        case this.STATES.FORWARD_BOTTOM:
        case this.STATES.FORWARD_TOP: {
            let chargedRate = this.getChargedRate();
            if (this.isCharging && chargedRate > 0.1) {
                const scale = [
                    chargedRate * 0.5 + 1,
                    chargedRate * -0.5 + 1
                ];
                drawScaledRect(context, this.position.x, this.position.y,
                    this.dimension.width, this.dimension.height, this.fillStyle,
                    scale,
                    this.state === this.STATES.FORWARD_BOTTOM ? 'bottom' : 'top');

            } else {
                context.fillRect(this.position.x, this.position.y,
                    this.dimension.width, this.dimension.height);
            }
            break;
        }
        default: {
            break; 
        }
        }
        return this;
    }

    jump() {
        const jumpInitial = this.getChargedRate()
            * (this.vilocity.jumpSpeadMax - this.vilocity.jumpSpeadMin)
            + this.vilocity.jumpSpeadMin;
        switch(this.state) {
        case this.STATES.FORWARD_BOTTOM: {
            this.state = this.STATES.JUMP_UP;
            this.vilocity.y = -jumpInitial;
            break;
        }
        case this.STATES.FORWARD_TOP: {
            this.state = this.STATES.JUMP_DOWN;
            this.vilocity.y = jumpInitial;
            break;
        }
        }
        this.isCharging = false;
        return this;
    }

    charge() {
        if (!this.isCharging) {
            this.isCharging = true;
            this.chargedTime = 0;
        }
    }

    tick(deltaTime) {
        // charge
        if (this.isCharging) { this.chargedTime += deltaTime; }
        // move
        this.position.y += this.vilocity.y;
        // check state change
        switch(this.state) {
        case this.STATES.JUMP_UP: { // jumping up
            if (this.position.y <= this.boundary.top) { // reached top boundary
                this.state = this.STATES.FORWARD_TOP;
                this.vilocity.y = 0;
                this.position.y = this.boundary.top;
            } else {
                this.vilocity.y += this.gravity;
            }
            break;
        }
        case this.STATES.JUMP_DOWN: { // jumping down
            if (this.position.y >= this.boundary.bottom) { // reached bottom boundary
                this.state = this.STATES.FORWARD_BOTTOM;
                this.vilocity.y = 0;
                this.position.y = this.boundary.bottom;
            } else {
                this.vilocity.y -= this.gravity;
            }
            break;
        }
        }
        return this;
    }
}

class Boundary {
    constructor(config) {
        this.dimension = {...config.dimension};
        this.position = {...config.position};
        this.fillStyle = config.fillStyle;
    }

    draw(context) {
        const oldFill = context.fillStyle;
        context.fillStyle = 'black';
        context.fillRect(this.position.x, this.position.y,
            this.dimension.width, this.dimension.height);
        context.fillStyle = oldFill;
        return this;
    }
}

class Obstacle {
    constructor(config) {
        this.dimension = {...config.dimension};
        this.boundary = {...config.boundary};
        this.position = {...config.position};
        this.vilocity = {...config.vilocity};
        this.fillStyle = config.fillStyle;
    }

    draw(context) {
        const oldFill = context.fillStyle;
        context.fillStyle = this.fillStyle;
        context.fillRect(this.position.x, this.position.y,
            this.dimension.width, this.dimension.height);
        context.fillStyle = oldFill;
        return this;
    }

    tick() {
        this.position.x += this.vilocity.x;
    }

    getRightBoundary() {
        return this.position.x + this.dimension.width;
    }

    getBoundingBox() {
        return {
            min: {
                x: this.position.x,
                y: this.position.y
            },
            max: {
                x: this.position.x + this.dimension.width,
                y: this.position.y + this.dimension.height
            }
        };
    }
}

class Game {
    constructor() {
        // states
        this.STATES = {
            'READY': 'ready',
            'PLAYING': 'playing',
            'ENDED': 'ended',
        };
        this.state = this.STATES.READY;
        // timestamp
        this.totalTime = 0;
        this.lastTimestamp = window.performance.now();

        // init canvas
        const canvas = document.querySelector('.canvas--scene');
        this.width = canvas.width;
        this.height = canvas.height;
        this.context = canvas.getContext('2d');
        const playerCanvas = document.querySelector('.canvas--player');
        this.playerContext = playerCanvas.getContext('2d');
        this.playerContext.fillStyle = 'white';
        this.playerContext.fillRect(0, 0, this.width, this.height);

        // stage config
        this.config = {
            blockSize: 40,
            boundaryHeight: 80,
            maxChargedTime: 1000.0,
            gravity: 0.7,
            runSpeedMin: -1,
            runSpeedMax: -8,
            jumpSpeadMin: 16,
            jumpSpeadMax: 50
        };

        // init boundaries
        const topBoundaryConfig = {
            dimension: {
                width: this.width,
                height: this.config.boundaryHeight
            },
            position: {
                x: 0,
                y: 0
            },
            fillStyle: 'black'
        };
        this.boundaries = [];
        // add top boundary
        this.boundaries.push(new Boundary(topBoundaryConfig));
        // add bottom boundary
        const bottomBoundaryConfig = deepClone(topBoundaryConfig);
        bottomBoundaryConfig.position.y = this.height - this.config.boundaryHeight;
        this.boundaries.push(new Boundary(bottomBoundaryConfig));

        // init player
        this.player = this.createPlayer();

        // init obstacles
        this.lastObstacleRow = 1;
        this.obstacles = [[], []];

        // event dom selectors and listeners
        document.addEventListener('keydown', e=>{
            if (e.key === ' ') {
                this.onSpaceDown();
            }
        });
        document.addEventListener('keyup', e=>{
            if (e.key === ' ') {
                this.onSpaceUp();
            }
        });
        this.mainContainer = document.querySelector('.game-container');
        const startButtons = document.querySelectorAll('.start-button');
        startButtons.forEach(btn =>{
            btn.addEventListener('click', ()=>{
                this.startGame();
            });
        });

        //score
        this.scoreELement = document.querySelector('.score');
        this.bestScoreElement = document.querySelector('.best-score');
        this.score = 0;
        this.bestScore = 0;

        // bind this for tick as callback
        this.tick = this.tick.bind(this);
        // start game
        this.tick();
    }

    createPlayer() {
        const playerPosY = this.height - this.config.blockSize - this.config.boundaryHeight;
        const playerConfig = {
            canvasConfig: {width: this.width, height: this.height},
            maxChargedTime: this.config.maxChargedTime,
            dimension: {width: this.config.blockSize, height: this.config.blockSize },
            boundary: {top: this.config.boundaryHeight, bottom: playerPosY},
            position: {x: 160, y: playerPosY},
            vilocity: {x: this.config.runSpeedMax, y: 0,
                runSpeedMin: this.config.runSpeedMin,
                runSpeedMax: this.config.runSpeedMax,
                jumpSpeadMin: this.config.jumpSpeadMin,
                jumpSpeadMax: this.config.jumpSpeadMax},
            gravity: this.config.gravity,
            fillStyle: 'black'
        };
        return new Player(playerConfig);
    }

    spawnObstacles() {
        if (Math.random() + this.totalTime * 0.000005 < 0.99) {  // become harder with time passing
            return; 
        }
        const i = 1 - this.lastObstacleRow;//genRandomInteger(0, 1);// for one side
        let lastPosX = 0;
        if (this.obstacles[i].length > 0) { // some obstacle this side
            const last = this.obstacles[i][this.obstacles[i].length - 1];
            lastPosX = Math.ceil(last.getRightBoundary());
        }
        if (this.obstacles[1 - i].length > 0) { // some obstacle other side
            const last = this.obstacles[1 - i][this.obstacles[1 - i].length - 1];
            lastPosX = Math.ceil(last.getRightBoundary());
            if (this.width - lastPosX < 80) {return; } // leave enough space to be playable
        }
        const width = genRandomInteger(1, 1.8) * this.config.blockSize + this.totalTime * 0.002; // become harder with time passing
        // console.log(width);
        const height = genRandomInteger(1, 1.8) * this.config.blockSize + this.totalTime * 0.002;
        // console.log(height);
        // console.log(this.totalTime);
        const px = lastPosX + Math.ceil((this.width - lastPosX) / this.config.blockSize) * this.config.blockSize;
        const py = (i == 0) ? this.config.boundaryHeight : (this.height - this.config.boundaryHeight - height);
        this.obstacles[i].push(new Obstacle({
            dimension: {width: width, height: height } ,
            position: {x: px, y: py},
            vilocity: {x:  this.config.runSpeed},
            fillStyle: 'black'
        }));
        this.lastObstacleRow = i;
    }

    clearObstacles() {
        for (const obstacleGroup of this.obstacles) {
            while(obstacleGroup.length > 0 && obstacleGroup[0].getRightBoundary() <= 0) {
                obstacleGroup.shift();
            }
        }
    }

    detectCollision() {
        const playerBoundingBox = this.player.getBoundingBox();
        for (const obstacleGroup of this.obstacles) {
            for (const obstacle of obstacleGroup) {
                const box = obstacle.getBoundingBox();
                const dist = getDistance(playerBoundingBox.position, box);
                if (dist < playerBoundingBox.radius) {
                    return true;
                }
            }
        }
        return false;
    }

    onSpaceDown() {
        if (this.state === this.STATES.PLAYING) {
            this.player.charge();
        }
    }

    onSpaceUp() {
        if (this.state === this.STATES.PLAYING) {
            this.player.jump();
        }
    }

    updateState(newState) {
        for (const key in this.STATES) {
            this.mainContainer.classList.remove(this.STATES[key]);
        }
        this.mainContainer.classList.add(newState);
        this.state = newState;
    }

    updateScore() {
        const chargedRate = this.player.getChargedRate();
        const  score = chargedRate < 0.5 ? 1 : (chargedRate < 0.8 ? 2 : 3);
        this.score += score;
        const scoreText = this.score.toString().padStart(5, '0');
        this.scoreELement.innerText = scoreText;
        if (this.bestScore < this.score) {
            this.bestScore = this.score;
            this.bestScoreElement.innerText = scoreText;
        }
    }

    resetGame() {
        this.totalTime = 0;
        this.lastTimestamp = window.performance.now();
        this.score = 0;
        this.obstacles = [[], []];
        this.lastObstacleRow = 1;
        this.player = this.createPlayer();
    }

    startGame() {
        switch (this.state) {
        case this.STATES.PLAYING: {
            return;
        }
        case this.STATES.READY: {
            this.updateState(this.STATES.PLAYING);
            this.bgmAudio = document.getElementById('bgmaudio');
            this.bgmAudio.volume = 0.2;
            this.bgmAudio.muted = false;
            this.bgmAudio.play();
            break;
        }
        case this.STATES.ENDED: {
            this.resetGame();
            this.updateState(this.STATES.PLAYING);
            this.bgmAudio.currentTime = 0;
            this.bgmAudio.play();
        }
        }
    }

    update(deltaTime) {
        if (this.state !== this.STATES.PLAYING) {
            return;
        }
        this.player.tick(deltaTime);
        this.clearObstacles();
        const playerSpeed = this.player.getSpeed();
        for (const obstacleGroup of this.obstacles) {
            for (const obstacle of obstacleGroup) {
                obstacle.vilocity.x = playerSpeed;
                obstacle.tick();
            }
        }
        if (this.detectCollision()) { // colission detected, game over
            this.updateState(this.STATES.ENDED);
            this.bgmAudio.pause();
            return;
        }
        const lastSec = Math.floor(this.totalTime / 100);
        const newSec = Math.floor((this.totalTime + deltaTime) / 100);
        if (lastSec < newSec) {
            this.updateScore();
        }
        this.totalTime += deltaTime;
        this.spawnObstacles();
    }

    draw() {
        // clear rectangle of game 
        this.context.clearRect(0, 0, this.width, this.height);
        // draw boundaries
        for (const boundary of this.boundaries) {
            boundary.draw(this.context);
        }
        // draw palyer
        this.player.draw(this.playerContext, this.state === this.STATES.PLAYING);
        // draw obstacles
        for (const obstacleGroup of this.obstacles) {
            for (const obstacle of obstacleGroup) {
                obstacle.draw(this.context);
            }
        }
    }

    tick() {
        const newTimestamp = performance.now();
        this.update(newTimestamp - this.lastTimestamp);
        this.draw();
        this.lastTimestamp = newTimestamp;
        window.requestAnimationFrame(this.tick);
    }
}

// eslint-disable-next-line no-unused-vars
const game = new Game();